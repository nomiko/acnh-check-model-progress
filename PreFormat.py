print("Opening bug and fish files...")
f_file = open("example_fish.txt")
b_file = open("example_bug.txt")

fish_data = {}
bug_data = {}

f_line = f_file.read().split("\n")
b_line = b_file.read().split("\n")
for fish in f_line:
    if fish != "":
        fish_data[fish] = 0
for bug in b_line:
    if bug != "":
        bug_data[bug] = 0

f_file.close()
b_file.close()
print("Done processing. Saving to required format")
try:
    f_file = open("fish.txt", "x")
    b_file = open("bug.txt", "x")
except FileExistsError:
    f_file = open("fish.txt", "w")
    b_file = open("bug.txt", "w")

for fish in fish_data:
    f_file.write(fish + ", " + str(fish_data[fish]) + "\n")

for bug in bug_data:
    b_file.write(bug + ", " + str(bug_data[bug]) + "\n")

print("Done saving. Closing files")
f_file.close()
b_file.close()
print("Done running preformatting")
