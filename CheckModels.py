class CheckModels:

    fish_data = {}
    bug_data = {}
    fish_file = "fish.txt"
    bug_file = "bug.txt"
    last_creature = []

    def __init__(self):
        f_file = open(self.fish_file)
        b_file = open(self.bug_file)
        self.fish_data = self.extract_files(f_file)
        self.bug_data = self.extract_files(b_file)

    def extract_files(self, file):
        creatures = {}
        lines = file.read().split('\n')
        for line in lines:
            try:
                if line != "":
                    creature = line.split(",")[0]
                    count = int(line.split(",")[1])
                    creatures[creature] = count
            except IndexError:
                raise ImportError
        return creatures

    def write_to_files(self, creatures, type):

        file = open(type, 'w')

        for creature in creatures:
            file.write(creature + ", " + str(creatures[creature]) + "\n")

        file.close()

    def check_creature(self, creature):
        amount = 1
        if len(creature.split(",")) > 1:
            amount = int(creature.split(",")[1])
            creature = creature.split(",")[0]

        if creature in self.bug_data:
            self.last_creature = ["bug"]
            self.incrementCount(creature, self.bug_data, amount)
        elif creature in self.fish_data:
            self.last_creature = ["fish"]
            self.incrementCount(creature, self.fish_data, amount)
        else:
            print("You've already finished model for", creature)


    def incrementCount(self, creature, creatures, amount):
        self.last_creature.append(creature)
        self.last_creature.append(creatures[creature])
        creatures[creature] += amount
        if creatures[creature] >= 3:
            print("Congratulation! You've finished collecting enough for a model of " + creature)
            creatures.pop(creature)
        else:
            print("Current status: collected " + str(creatures[creature]) + " of 3")

    def check_completion(self, creature):
        if creature in self.bug_data:
            return "incomplete, still need " + str(3-self.bug_data[creature]) + " more"
        if creature in self.fish_data:
            return "incomplete, still need " + str(3-self.fish_data[creature]) + " more"
        else:
            return "complete"

    def print_all(self, creature):
        if creature == "bug":
            creatures = self.bug_data
        else:
            creatures = self.fish_data

        print("For " + creature + ": " + str(len(creatures)) + " left")

        for creature in creatures:
            spaces = (40 - len(creature))*" "
            print (creature + ", " + spaces, creatures[creature], "of 3")

    def undo(self):
        if not self.last_creature:
            print("Nothing to undo\n")
            return

        if self.last_creature[0] == "bug":
            self.bug_data[self.last_creature[1]] = int(self.last_creature[2])
        else:
            self.fish_data[self.last_creature[1]] = int(self.last_creature[2])
        print("undo complete, reverted " + self.last_creature[1] + " to count " + str(self.last_creature[2]) + "\n")
        self.last_creature = []

    def wrapup(self):
        print("\nSaving data...")
        self.write_to_files(self.bug_data, self.bug_file)
        self.write_to_files(self.fish_data, self.fish_file)
        print("Data all saved!\n")


def main():
    try:
        models = CheckModels()
    except FileNotFoundError:
        print("The required files could not be found. Make sure they are in the same folder as this Python file.")
        print("Please run the PreFormat.py file if you have not done so already!")
        return
    except ImportError:
        print("The files are either corrupted or not saved in required format.")
        print("Please format the files by running PreFormat with fish and bugs list!")
        return
    line = ""
    while line != "end":
        line = input("Main menu: n for new creature, pf for list of fishes left, pb for list of bugs left, c" +
                     " for checking specific creature completion, s to save, end to quit\n").lower()
        print("\n")
        if line == "pf" or line == "pb":
            if line == "pf":
                creature = "fish"
            else:
                creature = "bug"
            print("-----------------------------------------------------")
            models.print_all(creature)
            print("-----------------------------------------------------\n\n")

        if line == "n" or line == "c":
            print("type 'back' to go back to the main menu\n")
            if line == "c":
                while line != "back":
                    line = input("Enter the name of the bug or fish you want to check: ").lower()
                    if line != "back":
                        print(models.check_completion(line))
                    print("\n")
            if line == "n":
                while line != "back":
                    line = input("Please enter a bug or fish you caught and amount for multiples\n \
                        For example, \"anchovy\" or \"anchovy, 3\": ").lower()
                    if line == "undo":
                        models.undo()
                        continue
                    if line != "back":
                        models.check_creature(line)
                    print("\n")

        if line == "s":
          models.wrapup()

    models.wrapup()


if __name__ == "__main__":
    main()
