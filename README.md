Hello! This project is simply for personal usage but feel free to use it to track the record of your model progress as well!

This project requires Python3 and dedication for you to complete each and every (or some) models in the ACNH game.

I've provided with a list of template list of each bug and fish; you do not have to do all the work yourself ;)

Simply clone this repository:
`git clone https://gitlab.com/nomiko/acnh-check-model-progress.git`

In the git folder, the files that you need to interact with are as follows:
* CheckModels.py
* PreFormat.py
* example\_bug.txt
* example\_fish.txt

**If you have your own list of bugs and fish, simply copy them to example files**

Please first run 
```
Python3 PreFormat.py
```

Then you can run the main file
```
Python3 CheckModels.py
```

Please don't forget to end the program by typing in "end" in the main menu! Otherwise it will not save your progress.

In the Main Menu of CheckModels.py:
* pf for printing all the fishes that are left in your lsit
* pb for printing all the bugs that are left in your list
* c for checking status for individual creature completion
* n for adding new creature for progress

`pf` and `pb` are commands directly from the main menu and it will take you back to the main menu after printing

`c` and `n` are commands that take you into loops, so you will need to enter "back" to go back to the main menu.

**All inputs are case insensitive.**

All feedbacks are welcome! Please find me on Discord, nomiko#0644, and let me know on how to make this better experience for you!

(Sorry, this project will stay as Python only with limited UI :rosiebepis:)
